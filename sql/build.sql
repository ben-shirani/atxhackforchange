CREATE TABLE org
(
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`type` enum ('agency', 'business', 'non-profit') NOT NULL,
	`name` varchar(250) NOT NULL,
	`website_url` varchar(250) NULL,
	`physical_address` varchar(250) NULL,
	`phone` VARCHAR(20) NULL,
	`email` VARCHAR(100) NULL,
	`twitter` VARCHAR(100) NULL,
	`facebook` VARCHAR(100) NULL,
	`linkedin` VARCHAR(100) NULL,
	`num_employees` INTEGER(50) NULL,
	`date_founded` DATETIME NULL,
	`sector` varchar(250) NULL,
	`ceo_name` VARCHAR(250) NULL,
	`industry` VARCHAR(250) NULL,
	`partnerships` text NULL,
	`director_name` VARCHAR(250) NULL,
	`board_members` TEXT NULL,
	`industry_vertical` VARCHAR(500) NULL,
	`org_chart` VARCHAR(250) NULL,
	`agency_city` VARCHAR(250) NULL,
	`agency_county` VARCHAR(250) NULL,
	`agency_state` VARCHAR(250) NULL,
	`agency_governing_office` VARCHAR(250),
	`mission_statement` TEXT NULL
);

CREATE TABLE tag
(
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` VARCHAR(250) NOT NULL,
	`short_name` VARCHAR(250) NULL,
	`color` VARCHAR(250) NOT NULL
);

CREATE TABLE goal
(
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`number` INTEGER(10) NOT NULL,
	`name` VARCHAR(250) NOT NULL,
	`description` TEXT NULL
);

CREATE TABLE xref_org_goal
(
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`org_id` INTEGER NOT NULL,
	`goal_id` INTEGER NOT NULL,
	INDEX xref_org_goal_org_id_ind (org_id),
	INDEX xref_org_goal_goal_id_ind (goal_id),
    FOREIGN KEY (org_id)
        REFERENCES org(id)
        ON DELETE CASCADE,
	FOREIGN KEY (goal_id)
        REFERENCES goal(id)
        ON DELETE CASCADE
);

CREATE TABLE xref_org_tag
(
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`org_id` INTEGER NOT NULL,
	`tag_id` INTEGER NOT NULL,
	INDEX xref_org_tag_org_id_ind (org_id),
	INDEX xref_org_tag_tag_id_ind (tag_id),
	FOREIGN KEY (org_id)
		REFERENCES org(id)
		ON DELETE CASCADE,
	FOREIGN KEY (tag_id)
		REFERENCES tag(id)
		ON DELETE CASCADE
);
