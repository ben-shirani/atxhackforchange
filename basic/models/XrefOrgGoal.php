<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "xref_org_goal".
 *
 * @property int $id
 * @property int $org_id
 * @property int $goal_id
 *
 * @property Org $org
 * @property Goal $goal
 */
class XrefOrgGoal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'xref_org_goal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['org_id', 'goal_id'], 'required'],
            [['org_id', 'goal_id'], 'integer'],
            [['org_id'], 'exist', 'skipOnError' => true, 'targetClass' => Org::className(), 'targetAttribute' => ['org_id' => 'id']],
            [['goal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goal::className(), 'targetAttribute' => ['goal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'org_id' => 'Org ID',
            'goal_id' => 'Goal ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(Org::className(), ['id' => 'org_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoal()
    {
        return $this->hasOne(Goal::className(), ['id' => 'goal_id']);
    }
}
