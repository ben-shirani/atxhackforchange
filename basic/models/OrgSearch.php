<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Org;

/**
 * OrgSearch represents the model behind the search form of `app\models\Org`.
 */
class OrgSearch extends Org
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'num_employees'], 'integer'],
            [['type', 'name', 'website_url', 'physical_address', 'phone', 'email', 'twitter', 'facebook', 'linkedin', 'date_founded', 'sector', 'ceo_name', 'industry', 'partnerships', 'director_name', 'board_members', 'industry_vertical', 'org_chart', 'agency_city', 'agency_county', 'agency_state', 'agency_governing_office', 'mission_statement'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Org::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'num_employees' => $this->num_employees,
            'date_founded' => $this->date_founded,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'website_url', $this->website_url])
            ->andFilterWhere(['like', 'physical_address', $this->physical_address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'twitter', $this->twitter])
            ->andFilterWhere(['like', 'facebook', $this->facebook])
            ->andFilterWhere(['like', 'linkedin', $this->linkedin])
            ->andFilterWhere(['like', 'sector', $this->sector])
            ->andFilterWhere(['like', 'ceo_name', $this->ceo_name])
            ->andFilterWhere(['like', 'industry', $this->industry])
            ->andFilterWhere(['like', 'partnerships', $this->partnerships])
            ->andFilterWhere(['like', 'director_name', $this->director_name])
            ->andFilterWhere(['like', 'board_members', $this->board_members])
            ->andFilterWhere(['like', 'industry_vertical', $this->industry_vertical])
            ->andFilterWhere(['like', 'org_chart', $this->org_chart])
            ->andFilterWhere(['like', 'agency_city', $this->agency_city])
            ->andFilterWhere(['like', 'agency_county', $this->agency_county])
            ->andFilterWhere(['like', 'agency_state', $this->agency_state])
            ->andFilterWhere(['like', 'agency_governing_office', $this->agency_governing_office])
            ->andFilterWhere(['like', 'mission_statement', $this->mission_statement]);

        return $dataProvider;
    }
}
