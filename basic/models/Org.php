<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "org".
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $website_url
 * @property string $physical_address
 * @property string $phone
 * @property string $email
 * @property string $twitter
 * @property string $facebook
 * @property string $linkedin
 * @property int $num_employees
 * @property string $date_founded
 * @property string $sector
 * @property string $ceo_name
 * @property string $industry
 * @property string $partnerships
 * @property string $director_name
 * @property string $board_members
 * @property string $industry_vertical
 * @property string $org_chart
 * @property string $agency_city
 * @property string $agency_county
 * @property string $agency_state
 * @property string $agency_governing_office
 * @property string $mission_statement
 *
 * @property XrefOrgGoal[] $xrefOrgGoals
 * @property XrefOrgTag[] $xrefOrgTags
 */
class Org extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'org';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'name'], 'required'],
            [['type', 'partnerships', 'board_members', 'mission_statement'], 'string'],
            [['num_employees'], 'integer'],
            [['date_founded'], 'safe'],
            [['name', 'website_url', 'physical_address', 'sector', 'ceo_name', 'industry', 'director_name', 'org_chart', 'agency_city', 'agency_county', 'agency_state', 'agency_governing_office'], 'string', 'max' => 250],
            [['phone'], 'string', 'max' => 20],
            [['email', 'twitter', 'facebook', 'linkedin'], 'string', 'max' => 100],
            [['industry_vertical'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'website_url' => 'Website Url',
            'physical_address' => 'Physical Address',
            'phone' => 'Phone',
            'email' => 'Email',
            'twitter' => 'Twitter',
            'facebook' => 'Facebook',
            'linkedin' => 'Linkedin',
            'num_employees' => 'Num Employees',
            'date_founded' => 'Date Founded',
            'sector' => 'Sector',
            'ceo_name' => 'Ceo Name',
            'industry' => 'Industry',
            'partnerships' => 'Partnerships',
            'director_name' => 'Director Name',
            'board_members' => 'Board Members',
            'industry_vertical' => 'Industry Vertical',
            'org_chart' => 'Org Chart',
            'agency_city' => 'Agency City',
            'agency_county' => 'Agency County',
            'agency_state' => 'Agency State',
            'agency_governing_office' => 'Agency Governing Office',
            'mission_statement' => 'Mission Statement',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getXrefOrgGoals()
    {
        return $this->hasMany(XrefOrgGoal::className(), ['org_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getXrefOrgTags()
    {
        return $this->hasMany(XrefOrgTag::className(), ['org_id' => 'id']);
    }
}
