<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="org-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'website_url') ?>

    <?= $form->field($model, 'physical_address') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'twitter') ?>

    <?php // echo $form->field($model, 'facebook') ?>

    <?php // echo $form->field($model, 'linkedin') ?>

    <?php // echo $form->field($model, 'num_employees') ?>

    <?php // echo $form->field($model, 'date_founded') ?>

    <?php // echo $form->field($model, 'sector') ?>

    <?php // echo $form->field($model, 'ceo_name') ?>

    <?php // echo $form->field($model, 'industry') ?>

    <?php // echo $form->field($model, 'partnerships') ?>

    <?php // echo $form->field($model, 'director_name') ?>

    <?php // echo $form->field($model, 'board_members') ?>

    <?php // echo $form->field($model, 'industry_vertical') ?>

    <?php // echo $form->field($model, 'org_chart') ?>

    <?php // echo $form->field($model, 'agency_city') ?>

    <?php // echo $form->field($model, 'agency_county') ?>

    <?php // echo $form->field($model, 'agency_state') ?>

    <?php // echo $form->field($model, 'agency_governing_office') ?>

    <?php // echo $form->field($model, 'mission_statement') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
