<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Org */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="org-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList([ 'agency' => 'Agency', 'business' => 'Business', 'non-profit' => 'Non-profit', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'physical_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'linkedin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_employees')->textInput() ?>

    <?= $form->field($model, 'date_founded')->textInput() ?>

    <?= $form->field($model, 'sector')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ceo_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'industry')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'partnerships')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'director_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'board_members')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'industry_vertical')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'org_chart')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agency_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agency_county')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agency_state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agency_governing_office')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mission_statement')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
