<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orgs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Org', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'name',
            'website_url:url',
            'physical_address',
            //'phone',
            //'email:email',
            //'twitter',
            //'facebook',
            //'linkedin',
            //'num_employees',
            //'date_founded',
            //'sector',
            //'ceo_name',
            //'industry',
            //'partnerships:ntext',
            //'director_name',
            //'board_members:ntext',
            //'industry_vertical',
            //'org_chart',
            //'agency_city',
            //'agency_county',
            //'agency_state',
            //'agency_governing_office',
            //'mission_statement:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
