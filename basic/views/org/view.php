<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Org */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Orgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'name',
            'website_url:url',
            'physical_address',
            'phone',
            'email:email',
            'twitter',
            'facebook',
            'linkedin',
            'num_employees',
            'date_founded',
            'sector',
            'ceo_name',
            'industry',
            'partnerships:ntext',
            'director_name',
            'board_members:ntext',
            'industry_vertical',
            'org_chart',
            'agency_city',
            'agency_county',
            'agency_state',
            'agency_governing_office',
            'mission_statement:ntext',
        ],
    ]) ?>

</div>
