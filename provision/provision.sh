#!/bin/bash

# Main switch logic
if [ "$1" = "kick" ]
then
	#vagrant plugin uninstall vagrant-aws
	#vagrant plugin install vagrant-aws
	#vagrant box add aws https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box
	vagrant up --provider=aws --provision
else
	ansible-playbook playbooks/ATXHackForChange.yaml -i inventory.ini --extra-vars="username=ben.shirani@gmail.com"
fi
